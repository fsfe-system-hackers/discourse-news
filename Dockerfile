# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FROM php:7-apache

ARG DISCOURSE_API_KEY

ARG LDAP_BIND_PW

ARG LDAP_IP

# Set up PHP files

COPY . /var/www/html

RUN sed -i "s/SECRET_API_KEY/$DISCOURSE_API_KEY/" /var/www/html/config.php


# Set up Apache, and prepare to run rootless

COPY 000-default.conf /etc/apache2/sites-enabled/

RUN sed -i "s/LDAP_BIND_PW/$LDAP_BIND_PW/" /etc/apache2/sites-enabled/000-default.conf
RUN sed -i "s/LDAP_IP/$LDAP_IP/" /etc/apache2/sites-enabled/000-default.conf

RUN sed -i -r "s/Listen 80$/Listen 8080/" /etc/apache2/ports.conf

RUN chown -R 1001:1001 /var/log/apache2/

RUN a2enmod ldap authnz_ldap

USER 1001

CMD apache2-foreground
