<?php
/*
  SPDX-License-Identifier: CC0-1.0
  SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
*/
$config = [
  'url' => 'https://community.fsfe.org', // no / behind URL!
  'api_key' => 'SECRET_API_KEY',
  'api_user' => 'fsfe',
  'cat_prep' => '39',
  'cat_news' => '34',
  'cat_podcast' => '34',
  'tag_news' => 'news',
  'tag_podcast' => 'podcast'
];

?>
