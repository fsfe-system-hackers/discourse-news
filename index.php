<?php
/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
*/

/* load config. You normally don't want to edit something here */
require_once 'config.php';
$url = $config['url'] . '/';
$api_key = $config['api_key'];
$api_user = $config['api_user'];
$cat_prep = $config['cat_prep'];
$cat_news = $config['cat_news'];
$cat_podcast = $config['cat_podcast'];
$tag_news = $config['tag_news'];
$tag_podcast = $config['tag_podcast'];

$do = isset($_GET['do']) ? $_GET['do'] : false;
$body = isset($_POST['body']) ? $_POST['body'] : false;
$title = isset($_POST['title']) ? $_POST['title'] : false;
$link = isset($_POST['link']) ? $_POST['link'] : false;
$id_t = isset($_POST['id_t']) ? $_POST['id_t'] : false;
$id_p = isset($_POST['id_p']) ? $_POST['id_p'] : false;
$cat = isset($_POST['cat']) ? $_POST['cat'] : false;

/******************
 * GENERIC FUNCTIONS
 ******************/

function apicall($method, $endpoint, $data) {
  $jsondata = json_encode($data);

  $ch = curl_init($GLOBALS['url'] . $endpoint);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Api-Key: {$GLOBALS['api_key']}",
    "Api-User: {$GLOBALS['api_user']}"
  ));
  if ($method === "POST" ) {
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
  } elseif ($method === "DELETE") {
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
  } elseif ($method === "PUT") {
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
  }
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $result = curl_exec($ch);
  if (empty($result)) {
    return curl_getinfo($ch, CURLINFO_HTTP_CODE);
  } else {
    return json_decode($result, true);
  }
  curl_close($ch);
}

function backtoindex($message) {
  $_SESSION['message'] = $message;
  header("Location: /");
}

function endcall($output, $message = Null) {
  if (is_array($output)) {
    if (array_key_exists("errors", $output)) {
      masthead();
      echo "<div id='message'>ERROR during API request. Please see the error below.</div>";
      echo "<pre>";
      print_r($output);
      echo "</pre></body></html>";
      exit(1);
    } elseif (!is_null($message)) {
      backtoindex($message);
    }
  } else {
    backtoindex("$message ($output)");
  }
}

function masthead() {
  echo '
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Discourse News Topic Management</title>
  <link rel="stylesheet" type="text/css" href="/style.css" >
</head>
<body>
  <h1>Discourse News Topic Management</h1>
  <hr />
  <p>
    <a href="/">Start</a> |
    <a href="/?do=add">Add new topic</a>
  </p>
  <hr />
  <div id="message">';
  if ( ! empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    $_SESSION['message'] = NULL;
  }
  echo '</div>';
}

/******************
 * ACTION FUNCTIONS
 ******************/

function dn_list() {
  $data = array();
  $out = apicall("GET", "c/{$GLOBALS['cat_prep']}.json", $data);
  endcall($out);
  masthead();
  ?>
  <table>
    <tr>
      <th>ID</th>
      <th>Title</th>
      <th>Link</th>
      <th>Actions</th>
    </tr>
  <?php
  foreach($out['topic_list']['topics'] as $topic) {
    if ($topic['pinned'] !== true) {
      echo "<tr>";
      echo "<td><a href='" . $GLOBALS['url'] . "t/" . $topic['id'] . "' target='_blank'>" . $topic['id'] . "</a></td>";
      echo "<td>" . htmlspecialchars($topic['title']) . "</td>";
      echo "<td><code>" . $GLOBALS['url'] . "t/" . $topic['id'] . "</code></td>";
      ?>
      <td>
        <form action="?do=view" method="POST">
          <input name="id_t" value="<?php echo $topic['id']; ?>" type="hidden" />
          <input type="submit" value="View/Edit">
        </form>
        <form action="?do=delete" method="POST">
          <input name="id_t" value="<?php echo $topic['id']; ?>" type="hidden" />
          <input type="submit" value="Delete">
        </form>
        <form action="?do=publish" method="POST">
          <input name="id_t" value="<?php echo $topic['id']; ?>" type="hidden" />
          <button type="submit" name="cat" value="news">Publish (news)</button>
          <button type="submit" name="cat" value="podcast">Publish (podcast)</button>
        </form>
      </td>
      <?php
      echo "</tr>";
    }
  }
  echo "</table>";
}

function dn_delete($id_t) {
  $data = array();
  $out = apicall("DELETE", "t/$id_t.json", $data);
  endcall($out, "Topic $id_t successfully deleted");
}

function dn_add($title, $body, $link) {
  // concatenate link to body
  $body = $body . "\n\n" . $link;

  $data = array(
    "title" => $title,
    "category" => $GLOBALS['cat_prep'],
    "raw" => $body
  );
  $out = apicall("POST", "posts.json", $data);
  endcall($out, "New topic successfully added.");
}

function dn_view($id_t) {
  $data = array();
  $out = apicall("GET", "t/$id_t.json", $data);
  endcall($out);

  $title = $out['title'];
  $id_p = $out['post_stream']['posts'][0]['id'];
  $out = apicall("GET", "posts/$id_p.json", $data);
  endcall($out);
  $body = $out['raw'];

  masthead();
  ?>
  <form action="?do=edit" method="POST">
    <input name="id_t" value="<?php echo $id_t ?>" type="hidden" />
    <input name="id_p" value="<?php echo $id_p ?>" type="hidden" />
    <label for="title">Post title:</label><br />
    <input type="text" id="title" name="title" size="60" value="<?php echo htmlspecialchars($title); ?>" /><br /><br />
    <label for="body">Body text (Markdown):</label><br />
    <textarea id="body" name="body" cols="70" rows="10"><?php echo htmlspecialchars($body); ?></textarea><br /><br />
    <input type="submit" value="Submit">
  </form>
  <?php
}

function dn_edit($id_t, $id_p, $title, $body) {
  // Update topic's title
  $data = array(
    "title" => $title
  );
  $out = apicall("PUT", "t/-/$id_t.json", $data);
  endcall($out);

  // Update post's body
  $data = array(
    "raw" => $body,
    "edit_reason" => "Edited by Discourse News Topic Management tool"
  );
  $out = apicall("PUT", "posts/$id_p.json", $data);
  endcall($out, "Topic $id_t and its post $id_p successfully edited.");
}

function dn_publish($id_t, $cat) {
  // Update topic's category and tag
  if ($cat === "news") {
    $cat = $GLOBALS['cat_news'];
    $tag = $GLOBALS['tag_news'];
  } elseif ($cat === "podcast") {
    $cat = $GLOBALS['cat_podcast'];
    $tag = $GLOBALS['tag_podcast'];
  } else {
    die("Category unknown");
  }
  $data = array(
    "category_id" => $cat,
    "tags" => array($tag)
  );
  $out = apicall("PUT", "t/-/$id_t.json", $data);
  endcall($out);

  // Update topic's timestamp
  $data = array(
    "timestamp" => strtotime("now")
  );
  $out = apicall("PUT", "t/$id_t/change-timestamp", $data);
  endcall($out, "Topic $id_t successfully published.");
}

/******************
 * ACTUAL RUNNER
 ******************/

session_start();

if (!$do || $do === "list") {
  dn_list();
}

if ($do === "delete" && $id_t) {
  dn_delete($id_t);
}

if ($do === "add" && $title && $body && $link) {
  dn_add($title, $body, $link);
} elseif ($do === "add") {
  masthead();
  echo '
  <form action="?do=add" method="POST">
    <label for="title">Post title:</label><br />
    <input type="text" id="title" name="title" size="60" /><br /><br />
    <label for="body">Body text (Markdown):</label><br />
    <textarea id="body" name="body" cols="70" rows="10"></textarea><br /><br />
    <label for="link">Final language-independent link to the news:</label><br />
    <input type="text" id="link" name="link" placeholder="https://fsfe.org/news/2006/news-20060102-01.html" size="60" /><br /><br />
    <input type="submit" value="Submit">
  </form>';
}

if ($do === "view" && $id_t) {
  dn_view($id_t);
}

if ($do === "edit" && $id_t && $id_p && $title && $body) {
  dn_edit($id_t, $id_p, $title, $body);
}

if ($do === "publish" && $id_t && $cat) {
  dn_publish($id_t, $cat);
}

?>
</body>
</html>
