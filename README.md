<!--
SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/discourse-news/00_README)

# Discourse News Topic Management

This service makes the process of preparing discourse topics for the discussion of FSFE news/podcast items easy. It is meant to be used by the FSFE's PR team members.

## Background

On fsfe.org we have the possibility to include a discussion link for news/podcast items which enabled interested people to comment on our articles. This happens via our [Discourse instance](https://community.fsfe.org/c/discussion/fsfe-news/34), and the [discussion tag](https://wiki.fsfe.org/TechDocs/Mainpage/Editing#Comments) in the XHTML files

## Features

To prepare a news item, the PR person requires the URL of the new Discourse topic, so it has to be prepared somehow. This service does exactly that:

* See all (unpinned) topics in the special preparation category, only visible to certain people
* View and edit any topic in this category
* Create a new topic with title and body text
* Delete a topic in this category
* Publish it as a news item or podcast episode
  * Sets the desired target category
  * Sets the desired tag
  * Updates the timestamp to now

Afterwards, such a topic can no longer be managed by this service, but only directly in Discourse, until it is back in the preparation category.

## Usage

1. Open [discourse-news.fsfe.org](https://discourse-news.fsfe.org/)
2. Use your personal FSFE account to log in
3. Add a new topic or manage existing ones 

## License

This project is Free Software and REUSE compliant.
